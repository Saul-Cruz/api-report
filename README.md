# README #

step 1 virtualenv:
* virtualenv dj2.2
* source dj2.2/bin/activate

step 2 Django version:
* python3 -m pip install Django==2.2

step 3 Create project:
* django-admin.py startproject api-report

step 4 Install psycopg2:
* pip3 install psycopg2

step 5 database:
* psql=# CREATE DATABASE api-report
* open api-report/settings.py
    [line 73] DATABASES = {
                    'default': {
                        'ENGINE': 'django.db.backends.postgresql_psycopg2',
                        'NAME': 'api-report',
                        'USER': 'apps',
                        'PASSWORD': 'apps',
                        'HOST': 'localhost',
                        'PORT': 5432,
                                }
                            }
* python3 manage.py migrate

step 6 create user:
* python3 manage.py createsuperuser
* admin
  saul.cruz.web@gmail.com
  #pythonDJ

  step 7 run server:
  * python3 manage.py runserver

step 8 aplication django:
* mkdir apps
* touch __init__.py
* django-admin.py startapp reports
* add in settings.py in variable INSTALLED_APPS = [ 'apps.reports',

step 9 create model in models.py:
https://docs.djangoproject.com/en/3.0/topics/db/models/
class Test(models.Model):
# NOTA: django automaticamente agrega el campo id autoincrementable
# id_test = models.CharField(max_length=10,primary_key=True)
    nombre = models.CharField(max_length=50)
    edad = models.IntegerField()
    fecha = models.DateField()

step 10 make migrations:
* python3 manage.py makemigrations
* python3 manage.py migrate

step 11 register model in admin.py:
* from apps.reports.models import Test
  admin.site.register(Test)